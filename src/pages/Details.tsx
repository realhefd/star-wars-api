// import Profile from '../components/Profile'
// import React from 'react';
// import '../styles/App.css';

// const Details: React.FC<{}> = () => {
//   return (
//     <Profile />
//   );
// }

// export default Details;

import Profile from '../components/Profile'
import { useAppSelector, useAppDispatch } from '../app/hooks';
import { GET_PERSON_BY_NAME } from '../graphqlq/queries'
import Loader from '../components/Loader'
import { useQuery } from '@apollo/client';
import React, { useEffect } from 'react';
import { useHistory } from "react-router-dom";

const Details: React.FC<{}> = () => {
  const history = useHistory();
  const {pathname, state} = history.location
  const userName = pathname.match(/details\/(.*)/)?.[1];
  //console.log(history)

  
  /*const { loading, error, data } = useQuery(GET_PERSON_BY_NAME, { variables: { name: userName?.split('+').join(' ') }});
 
  if (loading) return <><Loader /></>;
  if (error) return <>Error! {error.message};</>


  const { results } = data.getPerson  */

  return <Profile {...state} />;
}

export default Details;
