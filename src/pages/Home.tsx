import { selectTotalCount } from '../features/people/totalCountSlice'
import { setTotalCount } from '../features/people/totalCountSlice'
import { useAppSelector, useAppDispatch } from '../app/hooks';
import { selectPeople } from '../features/people/peopleSlice';
import { setPeople } from '../features/people/peopleSlice'; 
import { selectPage } from '../features/page/pageSlice';
import { GET_ALL_PEOPLE } from '../graphqlq/queries'
import Pagination from '../components/Pagination'
import { PersonInterface } from '../interfaces';
import People from '../components/People'
import Loader from '../components/Loader'
import { useQuery } from '@apollo/client';
import React from 'react';

const Home: React.FC<{}> = () => {
  const totalCount = useAppSelector(selectTotalCount);
  const currentPage = useAppSelector(selectPage);
  const people = useAppSelector(selectPeople);
  const dispatch = useAppDispatch()

  const pagination = <Pagination totalCount={ totalCount } currentPage={ currentPage } />

  // const data = useAppSelector((data: any) => {
  //   console.log('regregrgregr', data)
  //   return data.people as PersonInterface
  // });

  const { loading, error, data } = useQuery(GET_ALL_PEOPLE, { variables: { page: currentPage }});
 
  if (loading) return <><Loader />{ pagination }</>;
  if (error) return <>Error! {error.message};</>

  const { count, results } = data.getPeople
  dispatch(setTotalCount(count));
  dispatch(setPeople(results));

  return <><People people={people} />{ pagination }</>;
}

export default Home;
