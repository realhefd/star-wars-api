export interface PersonInterface {
  name: string,
  height: string,
  mass: string,
  gender: string,
  homeworld: {
    name: string,
    rotation_period: string,
    orbital_period: string,
    diameter: string,
    population: string
  }
}

export interface PageInterface {
    number: number
}
