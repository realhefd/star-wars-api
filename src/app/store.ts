import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import pageReducer from '../features/page/pageSlice';
import peopleReducer from '../features/people/peopleSlice';
import loaderReducer from '../features/people/loaderSlice';
import totalCountReducer from '../features/people/totalCountSlice';

export const store = configureStore({
  reducer: {
    page: pageReducer,
    people: peopleReducer,
    isLoading: loaderReducer,
    totalCount: totalCountReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
