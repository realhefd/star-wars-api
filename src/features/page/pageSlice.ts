import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
export interface PageState {
  value: number;
  status: 'idle' | 'loading' | 'failed';
}

const initialState: PageState = {
  value: 1,
  status: 'idle',
};

export const pageSlice = createSlice({
  name: 'counter',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    nextPage: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.value += 1;
    },
    previousPage: (state) => {
      state.value -= 1;
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    actualPage: (state, action: PayloadAction<number>) => {
      state.value = action.payload;
    },
  }
});

export const { nextPage, previousPage, actualPage } = pageSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.page.value)`
export const selectPage = (state: RootState) => state.page.value;

export default pageSlice.reducer;