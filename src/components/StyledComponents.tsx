import styled from "styled-components";

export const Container = styled.div`
  justify-content: center;
  padding: 2rem 4rem 0;
  flex-direction: row;
  flex-wrap: wrap;
  display: flex;
`;

export const StyledCard = styled.div`
  &:hover {
    box-shadow: 0 5px 6px -3px #00000033, 0 9px 12px 1px #00000024, 0 3px 16px 2px #0000001f
  }
  box-shadow: 0 2px 4px -1px #00000033, 0 4px 5px 0 #00000024, 0 1px 10px 0 #0000001f;
  justify-content: flex-start;
  transition: box-shadow .5s;
  overflow-wrap: break-word;
  align-content: flex-start;
  background-color: #fff;
  word-break: break-word;
  flex-direction: row;
  border: 1px solid;
  border-color: #fff;
  border-radius: 4px;
  font-size: 15px;
  flex-wrap: wrap;
  flex: 5 5 auto;
  padding: 10px;
  display: flex;
  margin: 10px;
  height: auto;
`;

export const StyledCardContent = styled.div`
  justify-content: flex-start;
  align-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  flex-wrap: wrap;
  display: flex;
  width: 235px;
`;

export const StyledRoundedBtn = styled.button`
  letter-spacing: .0892857143em;
  transition-duration: .28s;
  background-color: #1e83ec;
  border-color: #1e83ec;
  border-radius: 30px;
  border-style: none;
  cursor: pointer;
  min-width: 85px;
  font-size: 14px;
  padding: 0 15px;
  height: 30px;
  color: #fff;
  &:hover, &:focus {
    opacity:.7
  }
`;

export const StyledSpan = styled.span`
  font-family: Roboto,sans-serif;
  overflow-wrap: break-word;
  text-overflow: ellipsis;
  letter-spacing: normal;
  white-space: nowrap;
  align-self: center;
  font-size: 1.5rem;
  text-align: start;
  overflow: hidden;
  line-height: 1.2;
  flex: auto 1 auto;
`;

export const Avatar = styled.div`
  justify-content: center;
  vertical-align: middle;
  display: inline-flex;
  line-height: normal;
  align-items: center;
  text-align: center;
  border-radius: 50%;
  position: relative;
  overflow: hidden;
  min-width: 87px;
  height: 87px;
  width: 87px;

  img {
    border-radius: inherit;
    display: inline-flex;
    border-style: none;
    height: inherit;
    width: inherit;
  }
`;

export const PaginationBtnsWrapper = styled.div`
  margin: 0 15px;
`;

export const PaginationBtn = styled.button`
  border: 1px solid #1e83ec57;
  background-color: #1e83ec1a;
  padding: .3rem .75rem;
  transition: all 0.5s;
  border-radius: 30px;
  line-height: 1.5;
  appearance: none;
  margin: 0px 5px;
  font-size: 16px;
  cursor: pointer;
  color: #1e83ec;
  outline: none;

  :hover, :focus, :active {
    border-color: #1e83ec;
    background-color: #1e83ecd9;
    color: #fff;
  }
`;