import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React, { lazy, Suspense } from 'react';
import Navbar from './Navbar'
import Loader from './Loader'
import '../styles/App.css';

const NotFoundPage = lazy(() => import('../pages/NotFound'));
const DetailsPage = lazy(() => import('../pages/Details'));
const HomePage = lazy(() => import('../pages/Home'));

const App: React.FC = () => {

  return (
    <div className="App">
      <Router>
        <Navbar />

        <Suspense fallback={<Loader/>}>
          <Switch>
            <Route exact path="/details/:name" component={DetailsPage} />
            <Route exact path="/" component={HomePage} />
            <Route exact component={NotFoundPage} />
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;
