import starwarsLogo from '../assets/starwars.jpg';
import sovtechLogo from '../assets/logo.png';
import { Link } from "react-router-dom";
import ThemeSwitch from './Switch';
import React from 'react';
import '../styles/App.css';

const Navbar: React.FC<{}> = () => {
  return(
    <header className="App-header">
      <Link to="/">
        <img src={sovtechLogo} className="App-logo" alt="logo" />
      </Link>
      <img src={starwarsLogo} className="App-logo" alt="StarWars" />
      <ThemeSwitch />
    </header>
  )
};

export default Navbar;
